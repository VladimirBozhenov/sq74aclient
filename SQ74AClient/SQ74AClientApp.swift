//
//  SQ74AClientApp.swift
//  SQ74AClient
//
//  Created by Vladimir Bozhenov on 15.11.2020.
//

import SwiftUI

@main
struct SQ74AClientApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
